package service;

public interface ConversaoTemperaturaInterface {
	public Double celsiusToFahrenheit(Double temperatura);

	public Double fahrenheitToCelsius(Double temperatura);
}
