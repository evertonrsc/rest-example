package service;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Stateless
@Path("/conversao")
public class ConversaoTemperaturaResource implements ConversaoTemperaturaInterface {
	public ConversaoTemperaturaResource() {
		// Default constructor
	}
	
	@GET
	@Path("/celsiusToFahrenheit")
	@Produces(MediaType.TEXT_PLAIN)
	@Override
	public Double celsiusToFahrenheit(@QueryParam("temperatura") Double temperatura) {
		return temperatura * 9.0 / 5.0 + 32;
	}

	@GET
	@Path("/fahrenheitToCelsius")
	@Produces(MediaType.TEXT_PLAIN)
	@Override
	public Double fahrenheitToCelsius(@QueryParam("temperatura") Double temperatura) {
		return (temperatura - 32) * 5.0 / 9.0;
	}
}
